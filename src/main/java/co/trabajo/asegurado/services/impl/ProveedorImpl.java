package co.trabajo.asegurado.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindException;



import co.trabajo.asegurado.converter.ProveedorConverter;
import co.trabajo.asegurado.domain.Proveedor;
import co.trabajo.asegurado.repository.ProveedorRepository;
import co.trabajo.asegurado.services.ProveedorService;

@Service
public class ProveedorImpl implements ProveedorService{
	@Autowired
	private ProveedorConverter converter;
	
	@Autowired
	private ProveedorRepository repository;
	
	@Override
	public List<Proveedor> listAll() {
		
		return converter.entityToModel(repository.findAll());
	}
	@Override
	public void save(Proveedor proveedor) throws BindException {
		repository.save(converter.modelToEntity(proveedor));
		
	}

	@Override
	public Proveedor findById(Long id) {
		
		return converter.entityToModel(repository.findById(id).orElse(null));
	}

	@Override
	public void deleteById(Long id) {
		
		repository.deleteById(id);
	}

	@Override
	public void update(Proveedor proveedor) throws BindException {
		
		repository.save(converter.modelToEntity(proveedor));
		
	}

	@Override
	public void updateById(Proveedor proveedor, long id) throws BindException {
		
		proveedor.setIdProveedor(id);
		repository.save(converter.modelToEntity(proveedor));
		
	}

	
	
	
	
	
	}



