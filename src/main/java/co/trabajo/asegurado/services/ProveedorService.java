package co.trabajo.asegurado.services;

import java.util.List;

import org.springframework.validation.BindException;



import co.trabajo.asegurado.domain.Proveedor;

public interface ProveedorService {

	List<Proveedor> listAll();
	void save(Proveedor proveedor) throws BindException;
	
	Proveedor findById(Long id);
	void deleteById(Long id);
	void update(Proveedor proveedor) throws BindException;
	void updateById(Proveedor proveedor, long id) throws BindException;

	

}
