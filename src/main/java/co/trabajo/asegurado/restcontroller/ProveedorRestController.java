package co.trabajo.asegurado.restcontroller;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;



import co.trabajo.asegurado.domain.Proveedor;
import co.trabajo.asegurado.services.ProveedorService;

@RestController
@RequestMapping("/api")

public class ProveedorRestController {
	
	@Autowired
	private ProveedorService proveedorService;
	
	
	
	
	@GetMapping("/listar/proveedor")
	public List<Proveedor> listAll(){
		return proveedorService.listAll();		
	}
	@GetMapping("/proveedor/{id}")
	public Proveedor findById(@PathVariable long id) {
		return proveedorService.findById(id);
	}
	@PostMapping("/insertar/proveedor")
	public ResponseEntity<?> insert(@RequestBody Proveedor proveedor) {
		Map<String,Object> response = new HashMap<>();
		HttpStatus status = HttpStatus.BAD_REQUEST; 
		
		
		if(proveedor.validarIntegridad() &&  proveedor.validarNulls())
		{
							
		try {
			
			proveedorService.save(proveedor);
			response.put("Mensaje", "La información se ha insertado exitosamente");
			status = HttpStatus.CREATED;
			
			
		} catch (BindException e) {
			response.put("Causa", e.getObjectName().toString());
			response.put("Mensaje", "No se pudo insertar la información");
			status = HttpStatus.BAD_REQUEST;
		
		}
		}
		response.put("Mensaje","Los datos no cumplen las reglas del negocio");
		
		return new ResponseEntity<>(response, status);
	}
		
	@DeleteMapping("eliminar/proveedor/{id}")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public void delete(@PathVariable long id) {
		proveedorService.deleteById(id);
	}
	
	
	@PutMapping("/actualizar/proveedor")
	public ResponseEntity<?> actualizar(@RequestBody Proveedor proveedor) {
		Map<String,Object> response = new HashMap<>();
		HttpStatus status = HttpStatus.BAD_REQUEST;
		
		if(proveedor.validarIntegridad() &&  proveedor.validarNulls())
		{
							
		try {
			proveedorService.update(proveedor);
			response.put("Mensaje", "La información se ha actualizado exitosamente");
			status = HttpStatus.CREATED;
		} catch (BindException e) {
			response.put("Causa", e.getObjectName().toString());
			response.put("Mensaje", "No se pudo insertar la información, la proveedor no existe");
			status = HttpStatus.BAD_REQUEST;
		}
		}
		response.put("Mensaje","Los datos no cumplen las reglas del negocio");
		return new ResponseEntity<>(response, status);
	}
	@PutMapping("/actualizar/proveedor/{id}")
	public ResponseEntity<?> actualizarById(@RequestBody Proveedor proveedor,@PathVariable long id) throws BindException {
		Map<String,Object> response = new HashMap<>();
		HttpStatus status;
		List<Proveedor> proveedores = listAll();
		for(int i=0; i< proveedores.size(); i++) {
	
			if(proveedores.get(i).getIdProveedor()==id)
			{
				proveedorService.updateById(proveedor, id);
				response.put("Mensaje", "La información se ha actualizado exitosamente");
				status = HttpStatus.CREATED;
				return new ResponseEntity<>(response, status);
			}
			
		}
			response.put("Mensaje", "No se pudo insertar la información, el proveedor  no existe");
			status = HttpStatus.BAD_REQUEST;
			return new ResponseEntity<>(response, status);
	}

	

}
