package co.trabajo.asegurado.util.validator;

import org.springframework.stereotype.Component;

import co.trabajo.asegurado.entity.ProveedorEntity;
import co.trabajo.asegurado.util.validator.generic.GenericValidator;
@Component
public class ProveedorValidator extends GenericValidator<ProveedorEntity> {
	
	public ProveedorValidator() {
		super();
	}

}
