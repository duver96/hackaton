package co.trabajo.asegurado.test;

import static  junit.framework.Assert.*;
import org.junit.Test;

import co.trabajo.asegurado.domain.Proveedor;
public class ProveedorTest {
	
	
	@Test
    public void esPalindromoTest(){
        Proveedor proveedor = new Proveedor(1,"oeeo",null,null,null);
        boolean esPalindromo = proveedor.esPalindromo();
        assertTrue(esPalindromo);
    }

    @Test
    public  void noEsPalindromoTest(){

        Proveedor proveedor = new Proveedor(1,"oeoe",null,null,null);
        boolean esPalindromo = proveedor.esPalindromo();
        assertFalse(esPalindromo);
    }

    /*@Test
    public void esDomingoTest(){
        Proveedor proveedor = new Proveedor(1,"oeeo",null,new GregorianCalendar(),null);
        boolean esDomingo = proveedor.esDomingo();
        assertTrue(esDomingo);
    }*/

    @Test
    public void proveedorEsValido(){
        Proveedor proveedor = new Proveedor(1,"CEIBA SOFTWARE",null,null,null);
        boolean esValido = proveedor.proveedorEsValido();
        assertTrue(esValido);

    }

    @Test
    public void proveedorNoEsValido(){
        Proveedor proveedor = new Proveedor(1,"oeoe",null,null,null);
        boolean esValido = proveedor.proveedorEsValido();
        assertFalse(esValido);

    }

    @Test
    public void menorTresVocalesTest() {
	Proveedor proveedor = new Proveedor(); 
	proveedor.setDireccion("AE");

	
        int cantidadReal = proveedor.contarVocales();
        int cantidadEsperada = 2;
        assertEquals(cantidadReal, cantidadEsperada);
    }

    @Test
    public void mayorTresVocalesTest(){
    	Proveedor proveedor = new Proveedor(); 
    	proveedor.setDireccion("AEI");
    	
        int cantidadReal = proveedor.contarVocales();
        int cantidadEsperada = 3;
        assertEquals(cantidadReal, cantidadEsperada);

    }

    @Test
    public  void prefijoValidoTest(){
    	Proveedor proveedor = new Proveedor(); 
    	proveedor.setDireccion("CL");
        
        boolean validezPrefijoReal = proveedor.validarPrefijo();
        assertTrue(validezPrefijoReal);
    }

    @Test
    public  void prefijoNoValidoTest(){
    	Proveedor proveedor = new Proveedor(); 
    	proveedor.setDireccion("oe");
    	
        boolean validezPrefijoReal = proveedor.validarPrefijo();
        assertFalse(validezPrefijoReal);
    }


}
