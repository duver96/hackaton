package co.trabajo.asegurado;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SuTrabajoSeguro5Application {

	public static void main(String[] args) {
		SpringApplication.run(SuTrabajoSeguro5Application.class, args);
	}

}
