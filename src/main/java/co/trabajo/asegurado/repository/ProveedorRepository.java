package co.trabajo.asegurado.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import co.trabajo.asegurado.entity.ProveedorEntity;

public interface ProveedorRepository extends JpaRepository<ProveedorEntity, Serializable> {

}
