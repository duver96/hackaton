package co.trabajo.asegurado.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindException;



import co.trabajo.asegurado.domain.Proveedor;
import co.trabajo.asegurado.entity.ProveedorEntity;
import co.trabajo.asegurado.util.validator.ProveedorValidator;
@Component
public class ProveedorConverter {
	
	
	@Autowired
	private ProveedorValidator validator;
	
	public ProveedorEntity modelToEntity(Proveedor proveedor) throws BindException {
		
		ProveedorEntity proveedorEntity = new ProveedorEntity();
		proveedorEntity.setIdProveedor(proveedor.getIdProveedor());
		proveedorEntity.setDireccion(proveedor.getDireccion());
		proveedorEntity.setProveedor(proveedor.getProveedor());
		proveedorEntity.setTelefono(proveedor.getTelefono());
		
		validator.validate(proveedorEntity);

		return proveedorEntity;
	}
	public Proveedor entityToModel(ProveedorEntity proveedorEntity)
	{
		Proveedor proveedor = new Proveedor();
		proveedor.setIdProveedor(proveedorEntity.getIdProveedor());
		proveedor.setDireccion(proveedorEntity.getDireccion());
		proveedor.setProveedor(proveedorEntity.getProveedor());
		proveedor.setTelefono(proveedorEntity.getTelefono());
		return proveedor;
	}
	
	public List<Proveedor> entityToModel(List<ProveedorEntity> proveedorEntity) {
		List<Proveedor> proveedor = new ArrayList<Proveedor>(proveedorEntity.size());
		proveedorEntity.forEach((entity) -> {
			proveedor.add(entityToModel(entity));
		});
		return proveedor;
	}

}
