package co.trabajo.asegurado.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import javax.persistence.Table;



import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Table(name="proveedor")
@NoArgsConstructor
public class ProveedorEntity {
	@Id
    @GeneratedValue
    @Column(name = "id_proveedor")
    
	private long idProveedor;

	
    @Column(name = "poveedor", nullable = false, length = 30)
    private String proveedor;

    //@Column(name=fechaIngreso, nullable = false)
    //private GregorianCalendar fechaIngreso;
    
    
    @Column(name = "id_telefono" , nullable = false, length = 11)
    private String telefono;

   
    @Column(name = "id_direccion", nullable = false, length = 50)
    private String direccion;

}
