package co.trabajo.asegurado.domain;



import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Proveedor {
	
    
	private long idProveedor;
		
    private String proveedor;

    private String telefono;
    
    private Date fecha;
    

    private String direccion;
    
   // Calendar
    //Date
    public boolean validarNulls()
    {
    	if(proveedor ==null|| telefono==null || direccion == null 
    			||proveedor.trim().isEmpty() || telefono.trim().isEmpty()|| direccion.trim().isEmpty())
    	{
    		return false;
    	}
    	return true;
    	
    }
    
    public boolean validarIntegridad() {
    	
    	if(proveedorEsValido()&& validarPrefijo() &&contarVocales()>=3) {
    		
    		return true;
    	}
    	return false;
    	
    }
    public boolean esPalindromo() {
        for (int i = 0; i < proveedor.length(); i++) {
            if (proveedor.charAt(i) != proveedor.charAt(proveedor.length() - i-1)) {
                return false;
            }
        }
        return true;
    }

    public boolean proveedorEsValido(){
        if(!esPalindromo() && proveedor.length()>5){
            return true;
        }
        return false;
    }
   /* public boolean esDomingo(){
        boolean esDomingo = false;
        Calendar calendar = Calendar.getInstance();
        
        if(fecha.DAY_OF_WEEK== 0 ){
            esDomingo = true;
        }
        return esDomingo;
    }*/
    
    public boolean validarPrefijo(){
        boolean esValido = false;
        if(direccion.charAt(0) == 'C' && direccion.charAt(1) =='L'){
            esValido =true;
        }
        return esValido;
    }

    public int contarVocales(){
        int numeroVocales =0;
        for(int i =0; i<this.direccion.length(); i++){
            if( direccion.toUpperCase().charAt(i) == 'A' || direccion.toUpperCase().charAt(i) == 'E' || direccion.toUpperCase().charAt(i) == 'I' || direccion.toUpperCase().charAt(i) == 'O' || direccion.toUpperCase().charAt(i) == 'U'){
                numeroVocales +=1;
            }

        }
        return numeroVocales;
    }
    
    


}
